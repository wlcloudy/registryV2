# 使用SSL验证和Nginx做代理搭建生产环境的Docker仓库

  搭建私仓可以更方便、更安全的管理镜像资源，docker registry v2.0使用go语言编写，在性能安全上有很大的优化。
  
  一、环境准备
        我的环境：Ubuntu16.04 
        docker版本：Docker version 17.12.0-ce（docker安装参考官网:https://docs.docker.com/install/）
        
  二、安装docker compose（1.5.2） 和 registry
  
        1.安装 docker compose（官方文档：https://docs.docker.com/compose/overview/）
        
          $ curl -L https://github.com/docker/compose/releases/download/1.5.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
          $ chmod +x /usr/local/bin/docker-compose
          $ ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose 
          
        2.安装 htpasswd 
        
          htpasswd是Apache附带的程序，htpasswd生成包含用户名和密码的文本文件，每行内容格式为“用户名:密码”，用于用户文件的基本身份认证。
          如果你的web服务器是apache的话，那么应该会自带htpasswd程序，则忽略。
          
          $ apt-get install apache2-utils
          
  三、编写docker-compose.yml运行registry并使用ngnix做代理
    
        1.创建目录（和配置文件一致）
          $ mkdir /data/programs/docker -p   //创建目录
          $ cd /data/programs/docker
          $ mkdir data && mkdir nginx
          
        2.创建docker-compose.yml文件
          $ vim docker-compose.yml  //查看项目docker-compose.yml文件
          
        3.配置nginx
          在nginx目录中创建registry.conf文件配置nginx。配置nginx与registry的关系，转发端口，以及其他nginx的配置选项。
          $ cd /data/programs/docker/nginx
          $ vim registry.conf   //查看项目registry.conf文件
          
        4.执行docker-compose.yml文件
          $ cd /data/programs/docker
          $ docker-compose up
          
          执行docker-compose up后。注意是否有容器启动失败的消息，如果容器启动失败的消息，需要检查网络，是否能从dockerhub
          上pull image（需代理，或使用使用国内镜像，使用国内镜像需更改docker-compose.yml文件中image项）。也由可能粘贴配置
          文件错误，需仔细检查。启动后也可以使用docker ps命令查看是否两个容器都正常运行。
        
          #检验：都是返回{}
          $ curl http://localhost:5000/v2/  
          $ curl http://localhost:443/v2/  
          
          使用ctrl-c退出docker-compose，继续后面的步骤
      
        5.验证
          $ htpasswd -c registry.password docker //添加用户名和密码
          #修改registry.conf
          auth_basic "registry.localhost"
          auth_basic_user_file /etc/nginx/conf.d/registry.password
          add_header 'Docker-Distribution-Api-Version' 'registry/2.0' always
          $ cd /data/programs/docker
          $ docker-compose up
          $ curl http://localhost:5000/v2/  #结果：{}
          $ curl http://localhost:443/v2/   #结果提示：401 Authorization Required
          $ curl http://docker:123456@localhost:443/v2/ #结果：{}
          
  四、加入SSL验证    
    
        1.如果你有经过认证机构认证的证书，则直接使用将证书放入nginx目录下即可。如果没有，则使用openssl创建自己的证书。 
          $ cd /data/programs/docker/nginx
          $ openssl genrsa -out devdockerCA.key 2048 //生成根证书,一路回车即可
          $ openssl req -x509 -new -nodes -key devdockerCA.key -days 10000 -out devdockerCA.crt //为server创建一个key
          $ openssl genrsa -out domain.key 2048
          
          $ openssl req -new -key domain.key -out dev-docker-registry.com.csr
          
        　制作证书签名请求。注意在执行下面命令时，命令会提示输入一些信息，”Common Name”一项一定要输入你的域名（官方说IP也行，
        　但是也有IP不能加密的说法），其他项随便输入什么都可以。不要输入任何challenge密码，直接回车即可。 
        
          $ openssl x509 -req -in dev-docker-registry.com.csr -CA devdockerCA.crt -CAkey devdockerCA.key -CAcreateserial -out domain.crt -days 10000 //签署认证请求 
          
        2.配置nginx使用证书   
          #修改registry.conf配置文件，取消如下三行的注释 
          ssl on
          ssl_certificate /etc/nginx/conf.d/domain.crt
          ssl_certificate_key /etc/nginx/conf.d/domain.key
          
          # 再次执行docker-compose    
          $ cd /data/programs/docker
          $ docker-compose up
          $ curl http://localhost:5000/v2/  #结果：{}
          $ curl http://localhost:443/v2/   #结果提示：400 The plain HTTP request was sent to HTTPS port
          $ curl https://localhost:443/v2/  #应该使用https协议 
          
          由于是使用的未经任何认证机构认证的证书，并且还没有在本地应用自己生成的证书。所以此时会提示使用的是未经认证的证书，可以使用“-k”选项不进行验证。 
          
          $ curl -k https://localhost:443/v2/ #结果提示：401 Authorization Required
          
  五、Docker客户端使用registry 
    
          #安装ca-certificates包
          $ yum install ca-certificates
          
          #使能动态CA配置功能
          $ update-ca-trust force-enable 
          
          #将key拷贝到/etc/pki/ca-trust/source/anchors/
          $ cp devdockerCA.crt /etc/pki/ca-trust/source/anchors/
          
          #使新拷贝的证书生效
          $ update-ca-trust extract
          
          #证书拷贝后，需要重启docker以保证docker能使用新的证书
          $ service docker restart
          
          #Docker pull/push image测试
          #制作要push到registry的镜像
          
          #查看本地已有镜像
          $ docker images
          
          #为本地镜像打标签
          $ docker tag registry:2 docker-registry.com/registry:2
          $ docker ps //查看仓库及nginx容器是否启动
          $ docker login XXXXXXX（私仓地址,例如 https://registry.xxxx.com）
          $ docker push XXXXXX.com/registry //推送镜像私仓
          
          